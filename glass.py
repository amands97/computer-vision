import cv2
import math
face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
eye_cascade = cv2.CascadeClassifier('haarcascade_mcs_eyepair_big.xml')
nose_cascade = cv2.CascadeClassifier('nose2.xml')

imgMustache = cv2.imread('mustache.png',-1)
orig_mask_m = imgMustache[:,:,3]
orig_mask_inv_m = cv2.bitwise_not(orig_mask_m)
imgMustache = imgMustache[:,:,0:3]
origMustacheHeight, origMustacheWidth = imgMustache.shape[:2]
imgGlasses = cv2.imread('glasses.png', -1)  
orig_mask = imgGlasses[:,:,3]
imgGlassesGray = cv2.cvtColor(imgGlasses, cv2.COLOR_BGR2GRAY)
orig_mask_inv = cv2.bitwise_not(orig_mask)
imgGlasses = imgGlasses[:,:,0:3]
origGlassesHeight, origGlassesWidth = imgGlasses.shape[:2]

video_capture = cv2.VideoCapture(0)
i = 0
faces = []
while True:
    ret, frame = video_capture.read()
    i = i + 1
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    if(i%3 == 0):
        faces = face_cascade.detectMultiScale(gray, 1.3, 5)
    
    #print (faces)
    for (x,y,w,h) in faces:
        cv2.rectangle(frame,(x,y),(x+w,y+h),(255,0,0),2)
        roi_gray = gray[y:y+h, x:x+w]
        roi_color = frame[y:y+h, x:x+w]
        nose = nose_cascade.detectMultiScale(roi_gray)
        for (nx,ny,nw,nh) in nose:
            cv2.rectangle(roi_color,(nx,ny),(nx+nw,ny+nh),(255,0,0),2)
            mustacheWidth =  int(math.ceil(3 * nw *0.6))
            mustacheHeight = int(math.ceil(mustacheWidth * origMustacheHeight * 0.76 / origMustacheWidth))
            x1 = nx - (mustacheWidth/4)
            x2 = nx + nw + (mustacheWidth/4)
            y1 = ny + nh - (mustacheHeight/2)
            y2 = ny + nh + (mustacheHeight/2)
            if x1 < 0:
                x1 = 0
            if y1 < 0:
                y1 = 0
            if x2 > w:
                x2 = w
            if y2 > h:
                y2 = h
            #Check boundary cases
            mustacheWidth = x2 - x1
            mustacheHeight = y2 - y1
            mustache = cv2.resize(imgMustache, (mustacheWidth,mustacheHeight), interpolation = cv2.INTER_AREA)
            mask = cv2.resize(orig_mask_m, (mustacheWidth,mustacheHeight), interpolation = cv2.INTER_AREA)
            mask_inv = cv2.resize(orig_mask_inv_m, (mustacheWidth,mustacheHeight), interpolation = cv2.INTER_AREA)
            roi = roi_color[y1:y2, x1:x2]
            roi_bg = cv2.bitwise_and(roi,roi,mask = mask_inv)
            roi_fg = cv2.bitwise_and(mustache,mustache,mask = mask)
            dst = cv2.add(roi_bg,roi_fg)
            roi_color[y1:y2, x1:x2] = dst
            break
        eyes = eye_cascade.detectMultiScale(roi_gray)
        for (ex,ey,ew,eh) in eyes:
            cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(0,255,0),2)
            glassesWidth = int(0.7*ew)
            glassesHeight = glassesWidth * origGlassesHeight / origGlassesWidth
            x1 = ex - (glassesWidth/4)
            x2 = ex + ew + (glassesWidth/4)
            #y1 = ey + eh - (glassesHeight/2)
            #y2 = ey + eh + (glassesHeight/2)
            y1 = int(ey + eh - 0.8*(glassesHeight))
            y2 = int(ey + eh + 0.2*(glassesHeight))
            if x1 < 0:
                x1 = 0
            if y1 < 0:
                y1 = 0
            if x2 > w:
                x2 = w
            if y2 > h:
                y2 = h
            glassesWidth = (x2 - x1)
            glassesHeight = y2 - y1

            glasses = cv2.resize(imgGlasses, (glassesWidth,glassesHeight), interpolation = cv2.INTER_AREA)
            mask = cv2.resize(orig_mask, (glassesWidth,glassesHeight), interpolation = cv2.INTER_AREA)
            mask_inv = cv2.resize(orig_mask_inv, (glassesWidth,glassesHeight), interpolation = cv2.INTER_AREA)

            roi = roi_color[y1:y2, x1:x2]
            roi_bg = cv2.bitwise_and(roi,roi,mask = mask_inv)
            roi_fg = cv2.bitwise_and(glasses,glasses,mask = mask)
            dst = cv2.add(roi_bg,roi_fg)
            roi_color[y1:y2, x1:x2] = dst
            break

    cv2.imshow('Video', frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

video_capture.release()
cv2.destroyAllWindows()
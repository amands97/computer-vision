from imutils import face_utils
import numpy as np
import argparse
import imutils
import dlib
import cv2
imgGlasses = cv2.imread('glasses.png', -1)  
orig_mask = imgGlasses[:,:,3]
imgGlassesGray = cv2.cvtColor(imgGlasses, cv2.COLOR_BGR2GRAY)
orig_mask_inv = cv2.bitwise_not(orig_mask)
imgGlasses = imgGlasses[:,:,0:3]
origGlassesHeight, origGlassesWidth = imgGlasses.shape[:2]

detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")
def rect_to_bb(rect):
	# take a bounding predicted by dlib and convert it
	# to the format (x, y, w, h) as we would normally do
	# with OpenCV
	x = rect.left()
	y = rect.top()
	w = rect.right() - x
	h = rect.bottom() - y
 
	# return a tuple of (x, y, w, h)
	return (x, y, w, h)

def shape_to_np(shape, dtype="int"):
	# initialize the list of (x, y)-coordinates
	coords = np.zeros((68, 2), dtype=dtype)
 
	# loop over the 68 facial landmarks and convert them
	# to a 2-tuple of (x, y)-coordinates
	for i in range(0, 68):
		coords[i] = (shape.part(i).x, shape.part(i).y)
 
	# return the list of (x, y)-coordinates
	return coords
video_capture = cv2.VideoCapture(0)
(x, y, w, h) = (0,0,0,0)
(a,b) = (0,0)
shape = []
temp_rects = []
# load the input image, resize it, and convert it to grayscale
i = 0
sum_c = 0
sum_d = 0
sum_e = 0
sum_f = 0
while True:
	#image = cv2.imread(args["image"])
	ret, image = video_capture.read()
	image = imutils.resize(image, width=400)
	gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
	i = i + 1
	# detect faces in the grayscale image
	rects = []
	if( i%1 ==0):
		rects = detector(gray, 1)
		temp_rects = rects
	if not rects:
		'''
		cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 2)
		for (a, b) in shape:
			cv2.circle(image, (a, b), 1, (0, 0, 255), -1)
			'''
		for (i2, rect2) in enumerate(temp_rects):
		# determine the facial landmarks for the face region, then
		# convert the facial landmark (x, y)-coordinates to a NumPy
		# array
			shape = predictor(gray, rect2)
			shape = face_utils.shape_to_np(shape)

			# convert dlib's rectangle to a OpenCV-style bounding box
			# [i.e., (x, y, w, h)], then draw the face bounding box
			(x, y, w, h) = face_utils.rect_to_bb(rect2)
			cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 2)

			# show the face number
			cv2.putText(image, "Face #{}".format(i + 1), (x - 10, y - 10),
				cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)

			# loop over the (x, y)-coordinates for the facial landmarks
			# and draw them on the image
			#for (a, b) in shape:
			#	cv2.circle(image, (a, b), 1, (0, 0, 255), -1)
			cv2.circle(image, (sum_c, sum_d), 1, (0, 0, 255), -1)
			cv2.circle(image, (sum_e, sum_f), 1, (0, 0, 255), -1)
			cv2.circle(image, (x, y), 1, (0, 0, 255), -1)
	for (i, rect) in enumerate(rects):
		# determine the facial landmarks for the face region, then
		# convert the facial landmark (x, y)-coordinates to a NumPy
		# array
		shape = predictor(gray, rect)
		shape = face_utils.shape_to_np(shape)

		# convert dlib's rectangle to a OpenCV-style bounding box
		# [i.e., (x, y, w, h)], then draw the face bounding box
		(x, y, w, h) = face_utils.rect_to_bb(rect)
		cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 2)

		# show the face number
		cv2.putText(image, "Face #{}".format(i + 1), (x - 10, y - 10),
			cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)

		# loop over the (x, y)-coordinates for the facial landmarks
		# and draw them on the image
		print(shape)
		print("done")
		#for (a, b) in shape:
		#	cv2.circle(image, (a, b), 1, (0, 0, 255), -1)
		
		sum_c = 0
		sum_d = 0
		sum_e = 0
		sum_f = 0
		
		for i in range(36,42):
			(c,d) = shape[i]
			sum_c = sum_c + c
			sum_d = sum_d + d
		for j in range(42,48):
			(e,f) = shape[j]
			sum_e = sum_e + e
			sum_f = sum_f + f
		sum_c =  (sum_c / 6)
		sum_d =  (sum_d / 6)
		sum_e =  (sum_e / 6)
		sum_f =  (sum_f / 6)
		cv2.circle(image, (sum_c, sum_d), 1, (0, 0, 255), -1)
		cv2.circle(image, (sum_e, sum_f), 1, (0, 0, 255), -1)
		cv2.circle(image, (x, y), 1, (0, 0, 255), -1)

		ew = sum_e - sum_c
		ex = sum_c 
		ey = sum_d
		'''
		glassesWidth = int(0.7*ew)
		glassesHeight = glassesWidth * origGlassesHeight / origGlassesWidth
		x1 = ex - (glassesWidth/4)
		x2 = ex + ew + (glassesWidth/4)
		#y1 = ey + eh - (glassesHeight/2)
		#y2 = ey + eh + (glassesHeight/2)
		y1 = int(ey- 0.8*(glassesHeight))
		y2 = int(ey +0.2*(glassesHeight))
		if x1 < 0:
			x1 = 0
		if y1 < 0:
			y1 = 0
		if x2 > w:
			x2 = w
		if y2 > h:
			y2 = h
		glassesWidth = (x2 - x1)
		glassesHeight = y2 - y1
		print(glassesHeight)
		print(glassesWidth)
		glasses = cv2.resize(imgGlasses, (glassesWidth,glassesHeight), interpolation = cv2.INTER_AREA)
		mask = cv2.resize(orig_mask, (glassesWidth,glassesHeight), interpolation = cv2.INTER_AREA)
		mask_inv = cv2.resize(orig_mask_inv, (glassesWidth,glassesHeight), interpolation = cv2.INTER_AREA)
		roi_color = image[y-50:y+h+50, x-50:x+w+50]
		roi = roi_color[y1:y2, x1:x2]
		roi_bg = cv2.bitwise_and(roi,roi,mask = mask_inv)
		roi_fg = cv2.bitwise_and(glasses,glasses,mask = mask)
		dst = cv2.add(roi_bg,roi_fg)
		roi_color[y1:y2, x1:x2] = dst
		'''
		(x1,y1) = shape(48)
		(x2,y2) = shape(54)
		for k in range(48,60,6):
			(t1,t2) = shape[k]
			cv2.circle(image, (t1, t2), 1, (0, 0, 255), -1)
		
	cv2.imshow('Video', image)
	if cv2.waitKey(1) & 0xFF == ord('q'):
			break
# show the output image with the face detections + facial landmarks
cv2.imshow("Output", image)
cv2.waitKey(0)